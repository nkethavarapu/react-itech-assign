import "./index.css";
import Services from "./dependencies/services";
import { Clients, Partners } from "./dependencies/partners";
import Toplist from "./dependencies/toplist";
import Highlights from "./dependencies/highlights";
import Maindiv from "./dependencies/maindiv";
import Header from "./dependencies/header";
import Success from "./dependencies/success";
import Footer from "./dependencies/footer";

function Main() {
  return (
    <>
      <Header />
      <Maindiv />
      <Services />
      <Success />
      <Clients />
      <Highlights />
      <Partners />
      <Toplist />

      <Footer/>
       
          
    </>
  );
}

export default Main;
