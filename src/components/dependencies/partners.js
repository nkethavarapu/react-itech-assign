var Partner_shortcuts = [
  {
    src: "https://www.dropbox.com/s/mk5ca04seizpf8l/aws.svg?raw=1",
    alt: "Work with AWS",
    title: "Our Work",
  },
  {
    src: "https://www.dropbox.com/s/r9utt5nj9k9m1t8/Dell.png?raw=1",
    alt: "Dell",
    title: "Work with Dell",
  },
  {
    src: "https://www.dropbox.com/s/umw9g0zgm1ecfvn/Intel.png?raw=1",
    alt: "intel",
    title: "Work with intell",
  },
  {
    src: "https://www.dropbox.com/s/x0hrha2dosey99z/ibm.png?raw=1",
    alt: "IBM",
    title: "Work with IBM",
  },
  {
    src: "https://www.dropbox.com/s/ekzu1wcki6jziay/Microsoft.svg?raw=1",
    alt: "Microsoft",
    title: "Work with Microsoft",
  },
  {
    src: "https://www.dropbox.com/s/lvl5cp14i3v0wgi/Nasscom.png?raw=1",
    alt: "Nasscom",
    title: "Work with Nasscom",
  },
  {
    src: "https://www.dropbox.com/s/h66k9jaaknxaum4/Samsung.png?raw=1",
    alt: "Samsung",
    title: "Work with Samsung",
  },
  {
    src: "https://www.dropbox.com/s/86cbtf78khj0q9z/Nvidia.png?raw=1",
    alt: "Nvidia",
    title: "Work with Nvidia",
  },
];

  var client_shortcuts = [
    {
      src: "https://www.dropbox.com/s/lmvtthec9yn0ti6/Allianz.png?raw=1",
      alt: "Allianz",
      title: "Work with Allianz",
    },
    {
      src: "https://www.dropbox.com/s/kotgq2u4qr34i2u/audi.jpg?raw=1",
      alt: "Audi",
      title: "Work with Audi",
    },
    {
      src: "https://www.dropbox.com/s/t5dapt3lkz7rdhe/BMW.png?raw=1",
      alt: "BMW",
      title: "Work with BMW",
    },
    {
      src: "https://www.dropbox.com/s/ocqbsbgj590ztyy/ESPN.png?raw=1",
      alt: "ESPN",
      title: "Work with ESPN",
    },
    {
      src: "https://www.dropbox.com/s/2maaqxijcmbaqxg/LG.png?raw=1",
      alt: "LG",
      title: "Work with LG",
    },
    {
      src: "https://www.dropbox.com/s/yn3gj203hrdjfu7/Logo_NIKE.png?raw=1",
      alt: "Nike",
      title: "Work with Nike",
    },
    {
      src: "https://www.dropbox.com/s/gfxa6exv7h1ro6q/Suzuki_logo.png?raw=1",
      alt: "Suzuki",
      title: "Work with Suzuki",
    },
    {
      src: "https://www.dropbox.com/s/b7vwmjf6e0owybv/Visa.svg?raw=1",
      alt: "Visa",
      title: "Work with Visa",
    },
  ];

function Partners() {
  
  return (
    <>
      <section id="partners" className="brand-logos">
        <h1 className="sec-heading">Our Partners</h1>
        <div>
          {Partner_shortcuts.map((data) => {
            return <Part src={data.src} alt={data.alt} title={data.title} />;
          })}
        </div>
      </section>
    </>
  );

  
}
function Clients(){
 
  return(
    <> <section id="revenue" className="brand-logos">
    <h1 className="sec-heading">
      We Drive Growth & Revenue for the Best Companies
    </h1>
    <div>
      {client_shortcuts.map((data) => {
        return <Part src={data.src} alt={data.alt} title={data.title} />;
      })}
    </div>
  </section></>
  )

}
function Part(props) {
  return (
    <a>
      <img src={props.src} alt={props.alt} title={props.title} />
    </a>
  );
}
export {Clients,Partners};
